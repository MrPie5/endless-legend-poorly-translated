from googletrans import Translator
translator = Translator(service_urls=[
    'translate.google.com',
    'translate.google.co.kr'
])

import time

languages = [
    'am',
    'ar',
    'az',
    'ca',
    'gl',
    'ha',
    'ig',
    'la',
    'te',
    'uz',
    'zu',
    'haw',
    'eo',
    'vi',
    'xh',
    'kn',
    'es',
    'hy',
    'sk',
    'kk',
    'ka',
    'bn',
    'en'
]

strings = [
    "Your presence is welcome. May you grow prosperous and fat.",
    "Hail to your grace! Let us hope that we become honorable acquaintances.",
    "Peace be to Auriga, and to you. May our conversations be both civil and frequent.",
    "Good day, fellow sufferer. May the agony make us all stronger.",
    "Well met! A fine day it is for commerce and conversation. In that order.",
    "Greetings. Let us see if our empires can live together in harmony.",
    "Hello, stranger. We hope that our dealings will be brief and peaceful.",
    "Join us! The world and the universe await the flowering of our purpose."
]

counter = 1
for s in strings:
    print("Translating string", counter)
    start = time.time()
    translation = translator.translate(s, dest='en')
    for language in languages:
        translation = translator.translate(translation.text, dest=language)
    end = time.time()
    #print(translation.text)
    print("Done. Took", end - start, "seconds")
    counter += 1