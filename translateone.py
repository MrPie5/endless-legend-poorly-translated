from googletrans import Translator
translator = Translator(service_urls=[
    'translate.google.com',
    'translate.google.co.kr'
])

import sys
from random import choice

languages = [
    'am',
    'ar',
    'az',
    'ca',
    'gl',
    'ha',
    'ig',
    'la',
    'te',
    'uz',
    'zu',
    'haw',
    'eo',
    'vi',
    'xh',
    'kn',
    'es',
    'hy',
    'sk',
    'kk',
    'ka',
    'bn',
    'ja'
]

langQueue = []

toTranslate = " ".join(sys.argv[1:])

print(toTranslate)

translation = translator.translate(toTranslate, dest='en')

for i in range(0,15):
    lang = choice(languages)
    languages.remove(lang)
    langQueue.append(lang)
    if(len(langQueue) > 2):
        languages.append(langQueue[0])
        langQueue.remove(langQueue[0])

    print("Translating into", lang)

    translation = translator.translate(translation.text, dest=lang)

translation = translator.translate(translation.text, dest='en')
print(translation.text)