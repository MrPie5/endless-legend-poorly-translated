from googletrans import Translator
translator = Translator(service_urls=[
    'translate.google.com',
    'translate.google.co.kr'
])

from sys import argv
import xml.etree.ElementTree as xml
from random import choice
import httpcore._exceptions

languages = [
    'am',
    'ar',
    'az',
    'ca',
    'gl',
    'ha',
    'ig',
    'la',
    'te',
    'uz',
    'zu',
    'haw',
    'eo',
    'vi',
    'xh',
    'kn',
    'es',
    'hy',
    'sk',
    'kk',
    'ka',
    'bn',
    'gd',
    'ja',
    'zh-CN'
]

langQueue = []

if len(argv) > 1 and argv[1].lower() == "assets":
    fileBaseName = "EF_Localization_Assets_Locales.xml"
else:
    fileBaseName = "EF_Localization_Locales.xml"

sourceFile = r"originals/" + fileBaseName
destinationFile = r"translated/" + fileBaseName

print("Beginning translation of " + sourceFile + " to " + destinationFile)

tree = xml.parse(sourceFile)
root = tree.getroot()

#print(root[0].text)

total = len(list(root))

counter = 1
for child in root:
    if child.tag == "LocalizationPair":
        try:
            print("Translating string %d of %d" % (counter, total))
            translation = translator.translate(child.text, dest='en')

            repeat = True
            repeatCounter = 0
            while(repeat):
                try:
                    # 15 times, pick a random language from the list
                    # Remove it from the list, and add it to langQueue
                    # The langQueue's purpose is so that we don't translate one language into itself,
                    # or translate from A to B and back to A. Once more than 2 languages are in langQueue,
                    # the first one can be returned to the main list.
                    # After langQueue is handled, translate into the chosen language.
                    for i in range(0,15):
                        language = choice(languages)
                        languages.remove(language)
                        langQueue.append(language)
                        if(len(langQueue) > 2):
                            languages.append(langQueue[0])
                            langQueue.remove(langQueue[0])

                        translation = translator.translate(translation.text, dest=language)
                    translation = translator.translate(translation.text, dest='en')

                    # Check if the final result looks like English, is different from the original, and does not start with "I-" (idk why that last part keeps happening, but it does),
                    # and if not try the process again
                    # Up to 5 repeats per line, to avoid getting stuck in a loop
                    if(translator.detect(translation.text).lang != "en" or child.text.strip(":;.,- ").lower() == translation.text.strip(":;.,- ").lower() or translation.text.startswith("I-")):
                        if(repeatCounter < 5):
                            # final result is not acceptable, try again
                            print(translation.text)
                            print("Trying again")
                            repeatCounter += 1
                            ### add certainty, check for or strip html entities
                        else:
                            # final result is not acceptable, but we've tried too many times.
                            # mark it for later and leave it unchanged
                            repeat = False
                            child.set('translated', 'no')
                            print("Tried too many times, skipping")
                    else:
                        # final result is English (or close enough)
                        # only update the string if we get here

                        # strip the right colons that it likes to add, unless the original string had one
                        if(child.text.rstrip(' ')[-1] != ':'):
                            translation.text = translation.text.rstrip(" :")

                        child.text = translation.text
                        repeat = False

                except (httpcore._exceptions.ReadTimeout, httpcore._exceptions.ReadError, httpcore._exceptions.ConnectError) as e:
                    # ReadError and ConnectError sometimes happens if my IP changes during the process.
                    # ReadTimeout seems to happen randomly, no clear cause.
                    # try again ¯\_(ツ)_/¯
                    print("Received error", e, ", attempting to try again")
            
            # save our progress after every 5 strings
            if(counter % 5 == 0):
                tree.write(destinationFile)
                print("saved")
        
        except TypeError as e:
            # TypeErrors occur here if a translation string is empty or self-closing (idk why some do that)
            # For these cases, we can just skip to the next string
            pass

        counter += 1

tree.write(destinationFile)